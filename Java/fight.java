import java.util.*;

public class fight {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();

		System.out.print("请输入你的英雄名：");
		String yxm = scan.next();
		System.out.println("正在打扫战场，请稍后……");
		System.out.println("打扫完成！开始游戏！");
		while(true){
			System.out.println("请输入" + yxm + "出：（1.石头，2.剪刀，3.布）");
			String j = scan.next();
			int d = rand.nextInt(3);
			if (d == 0 && j.equals("1")){
				System.out.println("你出的是：石头，对方出的是：石头，平局！");
			}else if(d == 1 && j.equals("1")){
				System.out.println("你出的是：石头，对方出的是：剪刀，你赢了！");
			}else if(d == 2 && j.equals("1")){
				System.out.println("你出的是：石头，对方出的是：布，你输了！");
			}else if(d == 0 && j.equals("2")){
				System.out.println("你出的是：剪刀，对方出的是：石头，你输了！");
			}else if(d == 1 && j.equals("2")){
				System.out.println("你出的是：剪刀，对方出的是：剪刀，平局！");
			}else if(d == 2 && j.equals("2")){
				System.out.println("你出的是：剪刀，对方出的是：布，你赢了！");
			}else if(d == 0 && j.equals("3")){
				System.out.println("你出的是：布，对方出的是：石头，你赢了！");
			}else if(d == 1 && j.equals("3")){
				System.out.println("你出的是：布，对方出的是：剪刀，你输了！");
			}else if(d == 2 && j.equals("3")){
				System.out.println("你出的是：布，对方出的是：布，平局！");
			}else{
				System.out.println("你输入错误了，请重试！");
			}
			System.out.print("是否继续？（y/n）");
			String cont = scan.next();
			if (cont.equals("n")) {
				break;
			}
		}
	}
}