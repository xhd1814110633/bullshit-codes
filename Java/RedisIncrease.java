    //业务逻辑，给key值+1,redisUtils.incr(key)即可搞定，满满工作量，他尽力了
	public Integer addAlarmPeriod(String alarmRuleId) {
        String alarmPeriodKey = "ALARM_PERIOD_" + alarmRuleId ;
        Integer alarmPeriodVal = 1;
        if(redisUtils.hasKey(alarmPeriodKey)) {
            String alarmPeriodValStr = String.valueOf(redisUtils.get(alarmPeriodKey));
            if(!StringUtils.isEmpty(alarmPeriodValStr)) {
                alarmPeriodVal = Integer.parseInt(alarmPeriodValStr) + 1;
                redisUtils.set(alarmPeriodKey, alarmPeriodVal);
            } else {
                redisUtils.set(alarmPeriodKey,alarmPeriodVal);
            }
        } else {
            redisUtils.set(alarmPeriodKey,alarmPeriodVal);
        }
        return alarmPeriodVal;
    }