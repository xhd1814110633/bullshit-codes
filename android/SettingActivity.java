public class SettingActivity extends androidx.appcompat.app.AppCompatActivity {
    @Override
    protected void onCreate(@androidx.annotation.Nullable android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        clearMemory();
        
    }

    // 清理垃圾
    private void clearMemory() {
        try {
            // 经理说加一个延时效果，时间5秒，让甲方给钱后期便于优化
            Thread.sleep(5000);
            android.widget.Toast.makeText(SettingActivity.this, "清理完成!", Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}