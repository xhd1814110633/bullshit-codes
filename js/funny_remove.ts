/*
  业务功能说明
  hover删除一个角色下面的样式，如果是删除的是当前选中的样式就选中下一个样式
*/


//明明一个方法解决的问题，硬生生拆了3个。。。
export function removeStyleHandle(styleId: string) {
  const {SpritePanelStore} = stores.ComponentsStore;
  const isActor = !!BInstance.getActor(SpritePanelStore.selectSpriteId);
  if(isActor) {
    removeActorStyle(SpritePanelStore.selectSpriteId, styleId);
  } else {
    removeSceneStyle(SpritePanelStore.selectSpriteId, styleId);
  }
}

export function removeActorStyle(actorId: string, styleId: string) {
  const actorStyleIds:string[] = BInstance.getActorStyleSortIdList(BInstance.getCurrentActorId());
  // 1、就感觉哪里怪怪的。。。。Array.find是坏了吗
  const findIndex = actorStyleIds.findIndex((element) =>  {return element === styleId}) 
  BInstance.removeEntityStyle([styleId])
  SInstance.removeActorStyle(actorId, styleId)
  const hasNext = actorStyleIds[findIndex]
  if(hasNext) {
    selectActorStyle(actorStyleIds[findIndex])
  }else {
    selectActorStyle(actorStyleIds[findIndex-1])
  }
}
// 2、actor和scene是业务上的区分，数据存储一毛一样，那这个函数好像跟上面的有啥区别，为啥要写两个？
export function removeSceneStyle(sceneId: string, styleId: string) {
  const actorStyleIds:string[] = BInstance.getActorStyleSortIdList(BInstance.getCurrentSceneId());
  const findIndex = actorStyleIds.findIndex((element) =>  {return element === styleId})
  BInstance.removeEntityStyle([styleId])
  SInstance.removeActorStyle(sceneId, styleId)
  const hasNext = actorStyleIds[findIndex]
  if(hasNext) {
    selectSceneStyle(actorStyleIds[findIndex])
  }else {
    selectSceneStyle(actorStyleIds[findIndex-1])
  }
}