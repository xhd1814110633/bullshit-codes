#!/bin/bash

# 首先看一下 compgen 的用法
# compgen：
    # 用法：compgen [-abcdefgjksuv] [-o 选项]  [-A 动作] [-G 全局模式] [-W 词语列表]  [-F 函数] [-C 命令] [-X 过滤模式] [-P 前缀] [-S 后缀] [词语]


# 当我们第一次使用 compgen -W 通过词语列表来输出简单可能的补全词时，它没有任何问题

    compgen -W "java javac rust python go gcc" g
    
    # 它会输出两个可能的补全：
        # gcc
        # go

# 如果你想为 java 添加一些参数补全，例如 -jar -classpath .. 等，并提供 -j 为可能的补全参考(目的是直接补全 -jar)

    compgen -W "-jar -classpath --help -version" -j

    # 它会输出非预期的内容 - bullshit
        # -jar
        # -classpath
        # --help
        # -version

    # 正确的写法是，通过 -- 来隔断，这样才能输出预期内容
        
        compgen -W "-jar -classpath --help -version" -- -j
        
        # -jar



# bullshit 关键点：
    # 补全脚本如果涉及参数形式的内容，一般形式写法可能会产生非预期行为
    
    # 来自 man bash 的解释，但它却没有与 compgen 写在一块

        # --        -- 标志选项的结束，禁止其余的选项处理。任何 -- 之后的参数将作为文件名和参数对待。参数 - 与此等价。